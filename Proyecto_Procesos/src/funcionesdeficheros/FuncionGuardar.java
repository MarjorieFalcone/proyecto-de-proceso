/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcionesdeficheros;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlexDaniiCx
 */
public class FuncionGuardar extends ArchivosDelSistema {

    private String Documento;
    private FileOutputStream salida;

    public FuncionGuardar(String Documento) {
        this.Documento = Documento;
    }

    @Override
    public String Funcion(File Archivo) {
        String Mensaje = null;
        try {
            FileWriter save = new FileWriter(Archivo + ".txt");
            save.write(Documento);
            save.close();
            Mensaje = "Se guardó con exito";
        } catch (IOException ex) {
            Logger.getLogger(FuncionGuardar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Mensaje;
    }

}