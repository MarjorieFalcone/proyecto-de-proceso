/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package funcionesdeficheros;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author AlexDaniiCx
 */
public abstract class ArchivosDelSistema extends javax.swing.JFrame{
    private JFileChooser seleccion;
    FileNameExtensionFilter filtro; 
    public ArchivosDelSistema(){
        this.filtro=new FileNameExtensionFilter("*.TXT", "txt");
        this.seleccion=new JFileChooser();
        seleccion.setFileFilter(filtro);
       
        
    }

    public JFileChooser getSeleccion() {
        return seleccion;
    }
    
    public abstract String Funcion(File Archivo);
    
}
