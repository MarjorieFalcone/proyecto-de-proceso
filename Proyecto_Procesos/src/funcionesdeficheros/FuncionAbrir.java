package funcionesdeficheros;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFileChooser;

public class FuncionAbrir extends ArchivosDelSistema{
    
    private JFileChooser seleccionar;
    private FileInputStream entrada;
    private FileOutputStream salida;

    public FuncionAbrir() {
        seleccionar = new JFileChooser();
    }

    public JFileChooser getSeleccionar() {
        return seleccionar;
    }
    
    @Override
    public String Funcion(File Archivo) {
        String documento = "";
        try {
            entrada = new FileInputStream(Archivo);
            int ascci;
            while ((ascci = entrada.read()) != -1) {
                char caracter = (char) ascci;
                documento += caracter;
            }
        } catch (IOException e) {
        }
        return documento;
    }
}
